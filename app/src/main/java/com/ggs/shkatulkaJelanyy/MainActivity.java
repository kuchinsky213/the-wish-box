package com.ggs.shkatulkaJelanyy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
Button button;
TextView works;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = findViewById(R.id.button);
        works = findViewById(R.id.works);

        works.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent work = new Intent(MainActivity.this, HowItWorks.class);
                startActivity(work);
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActivityChest.class);
                startActivity(intent);
            }
        });
    }
}